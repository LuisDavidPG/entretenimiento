<%-- 
    Document   : contenidos
    Created on : 08-dic-2019, 20:38:48
    Author     : Daniel_Hernandez
--%>
<%@page import="Entidades.Video"%>
<%@page import="Entidades.Pdf"%>
<%@page import="Entidades.Texto"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Test tecnico</title>
        <style>
            img{
                width:100%;
            }
        </style>
    </head>
    <body>
        <h1>Contenidos</h1>
        
         <c:forEach items="${listaDeContenidos}" var="contenido">
           <c:set var = "tipo" value ="${contenido.getClass().getName()}"/>
           <c:choose>
                    <c:when test="${tipo =='Entidades.Video'}">
                       <div>
                            <h2>Titulo:<c:out value="${contenido.titulo}"/></h2>
                            <h2>Genero:<c:out value="${contenido.genero}"/></h2>                      
                            <h2>Duracion:<c:out value="${contenido.duracion}"/> minutos</h2>
                            <h2>Autor:<c:out value="${contenido.autor}"/></h2>
                            <video width="320" height="240" controls>
                                <source src="<c:out value="${contenido.rutaVideo}"/>" type="video/mp4">
                                <source src="<c:out value="${contenido.rutaVideo}"/>"  type="video/ogg">
                                Your browser does not support the video tag.
                            </video>
                        </div>
                    </c:when>
                    <c:when test="${tipo =='Entidades.Pdf'}">
                        <div>
                            <h2>Titulo:<c:out value="${contenido.titulo}"/></h2>
                            <h2>Genero:<c:out value="${contenido.genero}"/></h2>
                            <h2>Paginas:<c:out value="${contenido.paginas}"/></h2>
                            <h2><a href="<c:out value="${contenido.rutaPdf}"/>">Ver pdf</a></h2>     
                         </div>
                    </c:when>
                    <c:when test="${tipo =='Entidades.Texto'}">
                        <div>
                            <h2>Titulo:<c:out value="${contenido.titulo}"/></h2>
                            <h2>Genero:<c:out value="${contenido.genero}"/></h2>
                            <h2>Paginas:<c:out value="${contenido.paginas}"/></h2>
                            <h2><a href="<c:out value="${contenido.rutaTexto}"/>">Ver texto</a></h2>     
                         </div>
                    </c:when>
                    <c:otherwise>
                        <h1>No se han encontrados contenidos</h1>
                   </c:otherwise>
           </c:choose>
        </c:forEach>

        <form action="CanalesServlet" method="post">
                <input type="submit" value="Explorar Canales">
        </form>
             
    </body>
</html>
