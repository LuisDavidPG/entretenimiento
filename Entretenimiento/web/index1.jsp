<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="Controladores.CanalesServlet"%>
<%@page import="Entidades.Canal"%>
<%@page import="java.util.ArrayList"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
    <head>
        <style>
            canal {
                width: 20%;
                height: 20%;
                display: block;
                background-color: #dddddd;  
                font-size: 30px;
              
            }
            img{
                width:100%;
            }
        </style>



    </head>

    <body>
        <h1>Canales</h1>

        <c:forEach items="${listaCanales}" var="canal">
          <canal>
              <h2><c:out value="${canal.titulo}"/></h2>
              <h2><c:out value="${canal.idioma}"/></h2>
              <a href="CanalesServlet?fc=listarSubcanales&id_canal=${canal.idCanal}">
                  <img src="${canal.ruta_imagen}" alt="imagen evento" style='height: 80%; width: 80%; object-fit: contain'>     
              </a>                  
          </canal>
           
      </c:forEach>
       <br>
          
        <form action="CanalesServlet" method="post">
            <input type="submit" value="Explorar Canales">
        </form>
           
       
      <form action="CanalesServlet?fc=crearCvs" method="post">
           <input type="submit" value="Descargar rating">
      </form>
    

</body>
</html>
