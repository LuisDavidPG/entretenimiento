/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import Entidades.Contenido;
import java.util.ArrayList;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;


public class JUnitTest {
    
    public JUnitTest() {
    }

    
    
    @Test
    public void calcularRating() {

        float rating = 0f;
        float incrementar = 0f;
        int contador = 0;
        
        ArrayList<Contenido> listaContenidos = new ArrayList<Contenido>();
        Contenido contenido1 = new Contenido();
        
        contenido1.setId_contenido(1);
        contenido1.setId_canal(2);
        contenido1.setRating(7);
       
        Contenido contenido2 = new Contenido();
        
        contenido2.setId_contenido(2);
        contenido2.setId_canal(2);
        contenido2.setRating(6);
        
        listaContenidos.add(contenido1);
        listaContenidos.add(contenido2);
        if (!(listaContenidos.isEmpty())) {

            for (Contenido contenido : listaContenidos) {
                
                incrementar = incrementar + (float)contenido.getRating();
                contador++;
            }
            rating = incrementar / contador;

        } 
        
        if(rating==6.5f){
            System.err.println("Exito");
        }
    }
  
}
