package Controladores;

import Entidades.BaseDatos;
import Entidades.Canal;
import Entidades.Contenido;
import Entidades.Pdf;
import Entidades.Texto;
import Entidades.Video;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class CanalesServlet extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

        String funcion = "";
        ArrayList<Canal> listaCanales;
        if (!(request.getParameter("fc") == null)) {
            funcion = request.getParameter("fc");
        }

        switch (funcion) {

            case "listarSubcanales":

                String id_canal = request.getParameter("id_canal");
                int idCanal = Integer.parseInt(id_canal);
                ArrayList<Canal> listaSubCanales = listarSubCanales(idCanal);

                /*
                   Si no es¡xisten subcanales recoge los contenidos  
                 */
                if (listaSubCanales.isEmpty()) {
                    ContenidoServlet contenidos = new ContenidoServlet();
                    ArrayList<Contenido> listaDeContenidos = new ArrayList<Contenido>();
                    try {
                        listaDeContenidos = contenidos.mostrarContenido(idCanal);
                        request.setAttribute("listaDeContenidos", listaDeContenidos);
                        RequestDispatcher rd = request.getRequestDispatcher("contenidos.jsp");
                        rd.forward(request, response);
                    } catch (SQLException ex) {
                        Logger.getLogger(CanalesServlet.class.getName()).log(Level.SEVERE, null, ex);
                    }

                } else {
                    request.setAttribute("listaCanales", listaSubCanales);
                    RequestDispatcher rd1 = request.getRequestDispatcher("index1.jsp");
                    rd1.forward(request, response);
                }

                break;

            case "crearCvs":

                try {
                    //Calcular rating de subcanales y canales padres
                    calcularRatingSubcanales();
                } catch (SQLException ex) {
                    Logger.getLogger(CanalesServlet.class.getName()).log(Level.SEVERE, null, ex);
                }

                String resultado = crearArchivoRating();
                response.setContentType("text/csv");
                response.setHeader("Content-Disposition", "attachment; filename=ratings.csv");
                OutputStream outputStream = response.getOutputStream();
                outputStream.write(resultado.getBytes());
                outputStream.flush();
                outputStream.close();
                break;

            default:
                listaCanales = buscarCanales();
                request.setAttribute("listaCanales", listaCanales);
                RequestDispatcher rd0 = request.getRequestDispatcher("index1.jsp");
                rd0.forward(request, response);
                break;
        }
    }

    public ArrayList<Canal> buscarCanales() {

        ArrayList<Canal> listaCanales = new ArrayList<Canal>();
        BaseDatos bd = new BaseDatos();

        try {

            Statement st = bd.getCon().createStatement();
            String consultarCanales = "SELECT c.titulo,i.idioma,c.ruta_imagen,c.rating,sc.id_subcanal,c.idcanal "
                    + "FROM canal c join subcanal sc on id_canal=c.idcanal join idioma i on i.ididioma=c.idioma "
                    + "GROUP BY id_canal;";
            ResultSet rs = st.executeQuery(consultarCanales);

            while (rs.next()) {

                Canal canal = new Canal();
                canal.setIdCanal(rs.getInt("c.idcanal"));
                canal.setIdioma(rs.getString("i.idioma"));
                canal.setTitulo(rs.getString("c.titulo"));
                canal.setRuta_imagen(rs.getString("c.ruta_imagen"));
                listaCanales.add(canal);
            }

            st.close();

            String consultarCanales2 = "select c.titulo,i.idioma,c.ruta_imagen,c.rating,c.idcanal from canal c "
                    + "join contenido co on c.idcanal=co.id_canal\n"
                    + "join idioma i on i.ididioma=c.idioma\n"
                    + " where not exists\n"
                    + "(select * from subcanal sc where c.idcanal=sc.id_subcanal);";

            Statement st2 = bd.getCon().createStatement();
            ResultSet rs2 = st2.executeQuery(consultarCanales2);

            while (rs2.next()) {

                Canal canal = new Canal();
                canal.setIdCanal(rs2.getInt("c.idcanal"));
                canal.setIdioma(rs2.getString("i.idioma"));
                canal.setTitulo(rs2.getString("c.titulo"));
                canal.setRuta_imagen(rs2.getString("c.ruta_imagen"));
                listaCanales.add(canal);
            }

            bd.cerrarConexion();

        } catch (SQLException ex) {
            Logger.getLogger(CanalesServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
        return listaCanales;
    }

    public ArrayList<Canal> listarSubCanales(int idCanal) {

        ArrayList<Canal> listarSubCanales = new ArrayList<Canal>();
        BaseDatos bd = new BaseDatos();

        try {

            PreparedStatement st;
            st = bd.getCon().prepareStatement("SELECT c.titulo,i.idioma,c.ruta_imagen,sc.id_subcanal,c.rating,c.idcanal FROM canal c join subcanal sc on c.idcanal=sc.id_subcanal and sc.id_canal=? join idioma i on i.ididioma=c.idioma;");

            st.setInt(1, idCanal);
            ResultSet rs = st.executeQuery();

            while (rs.next()){
                Canal canal = new Canal();
                canal.setIdCanal(rs.getInt("sc.id_subcanal"));
                canal.setIdioma(rs.getString("i.idioma"));
                canal.setTitulo(rs.getString("c.titulo"));
                canal.setRuta_imagen(rs.getString("c.ruta_imagen"));
                listarSubCanales.add(canal);
            }

            st.close();
            bd.cerrarConexion();

        } catch (SQLException ex) {
            Logger.getLogger(CanalesServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
        return listarSubCanales;
    }

    public void calcularRatingSubcanales() throws SQLException {

        BaseDatos bd = new BaseDatos();

        try {

            PreparedStatement st;
            st = bd.getCon().prepareStatement("SELECT AVG(co.rating) as ratingCalculado,co.id_canal from CONTENIDO co "
                    + "join canal c on c.idcanal=co.id_canal GROUP BY idCanal;");
            ResultSet rs = st.executeQuery();

            /*
            Actualizacion de la Base de datos para dar Rating a los Subchanels
             */
            while (rs.next()) {

                PreparedStatement st2;
                st2 = bd.getCon().prepareStatement("UPDATE canal SET rating = ? WHERE idcanal = ?;");
                st2.setFloat(1, rs.getFloat("ratingCalculado"));
                st2.setInt(2, rs.getInt("co.id_canal"));
                st2.executeUpdate();

            }
            /*
            Una vez tengamos calculado el rating de los subcanales calculamos el de los canales padres.
             */
            calcularRatingCanalesPadre();

        } catch (SQLException ex) {
            Logger.getLogger(CanalesServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void calcularRatingCanalesPadre() throws SQLException {

        BaseDatos bd = new BaseDatos();

        try {

            PreparedStatement st;
            st = bd.getCon().prepareStatement("SELECT AVG(c.rating) as ratingCalculadoCanalPadre,sc.id_canal from canal c join subcanal sc on c.idcanal= sc.id_subcanal join canal GROUP BY sc.id_canal;");
            ResultSet rs = st.executeQuery();

            /*
            Actualizacion de la Base de datos para dar Rating a los Subchanels
             */
            while (rs.next()) {

                PreparedStatement st2;
                st2 = bd.getCon().prepareStatement("UPDATE canal SET rating = ? WHERE idcanal = ?;");
                st2.setFloat(1, rs.getFloat("ratingCalculadoCanalPadre"));
                st2.setInt(2, rs.getInt("sc.id_canal"));
                st2.executeUpdate();
                st2.close();
            }

            bd.cerrarConexion();
        } catch (SQLException ex) {
            Logger.getLogger(CanalesServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public String crearArchivoRating() {

        BaseDatos bd = new BaseDatos();
        PreparedStatement st;
        String resultado = "";

        try {

            st = bd.getCon().prepareStatement("SELECT c.titulo,c.rating FROM canal c GROUP BY c.idcanal ORDER by c.rating DESC;");
            ResultSet rs = st.executeQuery();
            while (rs.next()) {

                String titulo = rs.getString("c.titulo");
                String rating = rs.getString("c.rating");
                resultado = resultado.concat(titulo + "," + rating + "\n");
            }

            st.close();
            bd.cerrarConexion();

            return resultado;

        } catch (SQLException ex) {
            Logger.getLogger(CanalesServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
        return resultado;
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
