package Controladores;

import Entidades.BaseDatos;
import Entidades.Contenido;
import Entidades.Pdf;
import Entidades.Texto;
import Entidades.Video;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class ContenidoServlet extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

        
    }

    public ArrayList<Contenido> mostrarContenido(int idCanal) throws SQLException {

        BaseDatos bd = new BaseDatos();
        PreparedStatement st;
        
        ArrayList<Contenido> listaContenido = new ArrayList<Contenido>();
        HashMap<Integer, Integer> listaTipoContenido = new HashMap<Integer, Integer>();

        st = bd.getCon().prepareStatement("SELECT ct.idTipo_contenido,ct.idcontenido FROM contenido ct join canal c on c.idcanal=? and ct.id_canal=?  "
                + "join tipo_contenido tc on ct.idTipo_contenido=tc.idtipo_contenido;");
        st.setInt(1, idCanal);
        st.setInt(2, idCanal);
        ResultSet rs2 = st.executeQuery();

        while (rs2.next()) {
            listaTipoContenido.put(rs2.getInt("ct.idTipo_contenido"), rs2.getInt("ct.idcontenido"));
        }

        for (Map.Entry<Integer, Integer> valores : listaTipoContenido.entrySet()) {

            if (valores.getKey() == 1) {

                st = bd.getCon().prepareStatement("SELECT v.titulo,v.autor,v.ruta,v.duracion,g.genero FROM video v join genero g on v.id_genero=g.idgenero where v.idvideo=?;");
                st.setInt(1, valores.getValue());
                ResultSet rs3 = st.executeQuery();

                while (rs3.next()) {
                    Video video = new Video();
                    video.setTitulo(rs3.getString("v.titulo"));
                    video.setAutor(rs3.getString("v.autor"));
                    video.setDuracion(rs3.getInt("v.duracion"));
                    video.setRutaVideo(rs3.getString("v.ruta"));
                    video.setGenero(rs3.getString("g.genero"));
                    listaContenido.add(video);

                }

            } else if (valores.getKey() == 2) {

                st = bd.getCon().prepareStatement("SELECT p.titulo,p.paginas,p.ruta,g.genero"
                        + " FROM pdf p join genero g on p.id_genero=g.idgenero where p.idpdf=?;");
                st.setInt(1, valores.getValue());
                ResultSet rs3 = st.executeQuery();

                while (rs3.next()) {
                    Pdf pdf = new Pdf();
                    pdf.setTitulo(rs3.getString("p.titulo"));
                    pdf.setPaginas(rs3.getInt("p.paginas"));
                    pdf.setGenero(rs3.getString("g.genero"));
                    pdf.setRutaPdf(rs3.getString("p.ruta"));
                    listaContenido.add(pdf);

                }

            } else if (valores.getKey() == 3) {

                st = bd.getCon().prepareStatement("SELECT t.titulo,t.paginas,t.autor,t.ruta,g.genero FROM texto t join genero g on t.id_genero=g.idgenero where t.idtexto=?;");
                st.setInt(1, valores.getValue());
                ResultSet rs3 = st.executeQuery();

                while (rs3.next()) {
                    Texto texto = new Texto();
                    texto.setTitulo(rs3.getString("t.titulo"));
                    texto.setAutor(rs3.getString("t.autor"));
                    texto.setPaginas(rs3.getInt("t.paginas"));
                    texto.setRutaTexto(rs3.getString("t.ruta"));
                    texto.setGenero(rs3.getString("g.genero"));
                    listaContenido.add(texto);

                }

            }

        }
        
        return listaContenido;
    }
    
   

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
