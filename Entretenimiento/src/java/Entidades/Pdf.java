
package Entidades;


public class Pdf extends Contenido{
    
    private int idPdf;
    private String titulo;
    private String genero;
    private String rutaPdf;
    private int paginas;
    private int id_canal;
    private int id_contenido;
    private int rating;

    public Pdf() {
    }
    
    

    public Pdf(int idPdf, String titulo, String genero, String rutaPdf, int paginas, int id_canal, int id_contenido, int rating) {
        this.idPdf = idPdf;
        this.titulo = titulo;
        this.genero = genero;
        this.rutaPdf = rutaPdf;
        this.paginas = paginas;
        this.id_canal = id_canal;
        this.id_contenido = id_contenido;
        this.rating = rating;
    }

    public int getIdPdf() {
        return idPdf;
    }

    public void setIdPdf(int idPdf) {
        this.idPdf = idPdf;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getGenero() {
        return genero;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }

    public String getRutaPdf() {
        return rutaPdf;
    }

    public void setRutaPdf(String rutaPdf) {
        this.rutaPdf = rutaPdf;
    }

    public int getPaginas() {
        return paginas;
    }

    public void setPaginas(int paginas) {
        this.paginas = paginas;
    }

    @Override
    public int getId_canal() {
        return id_canal;
    }

    @Override
    public void setId_canal(int id_canal) {
        this.id_canal = id_canal;
    }

    public int getId_contenido() {
        return id_contenido;
    }

    @Override
    public void setId_contenido(int id_contenido) {
        this.id_contenido = id_contenido;
    }

    @Override
    public int getRating() {
        return rating;
    }

    @Override
    public void setRating(int rating) {
        this.rating = rating;
    }

    @Override
    public String toString() {
        return "Pdf{" + "idPdf=" + idPdf + ", titulo=" + titulo + ", genero=" + genero + ", rutaPdf=" + rutaPdf + ", paginas=" + paginas + ", id_canal=" + id_canal + ", id_contenido=" + id_contenido + ", rating=" + rating + '}';
    }

    
    
    
    
}
