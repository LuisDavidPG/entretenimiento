package Entidades;


public class Texto extends Contenido{
    
    private int idTexto;
    private String titulo;
    private int paginas;
    private String autor;
    private String genero;
    private String rutaTexto;
    private int id_canal;
    private int id_contenido;
    private int rating;

    public Texto() {
    }

    public Texto(int idTexto, String titulo, int paginas, String autor, String genero, String rutaTexto) {
        this.idTexto = idTexto;
        this.titulo = titulo;
        this.paginas = paginas;
        this.autor = autor;
        this.genero = genero;
        this.rutaTexto = rutaTexto;
    }

    

    public int getIdTexto() {
        return idTexto;
    }

    public void setIdTexto(int idTexto) {
        this.idTexto = idTexto;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public int getPaginas() {
        return paginas;
    }

    public void setPaginas(int paginas) {
        this.paginas = paginas;
    }
    
    

   
    public String getAutor() {
        return autor;
    }

    public void setAutor(String autor) {
        this.autor = autor;
    }

    public String getGenero() {
        return genero;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }

    public String getRutaTexto() {
        return rutaTexto;
    }

    public void setRutaTexto(String rutaTexto) {
        this.rutaTexto = rutaTexto;
    }

    

    public int getId_canal() {
        return id_canal;
    }

    public void setId_canal(int id_canal) {
        this.id_canal = id_canal;
    }

    public int getId_contenido() {
        return id_contenido;
    }
    
    public void setId_contenido(int id_contenido) {
        this.id_contenido = this.idTexto;
    }

  
    public int getRating() {
        return this.rating;
    }

   
    public void setRating(int rating) {
        this.rating = rating;
    }

    @Override
    public String toString() {
        return "Texto{" + "idTexto=" + idTexto + ", titulo=" + titulo + ", paginas=" + paginas + ", autor=" + autor + ", genero=" + genero + ", rutaTexto=" + rutaTexto + ", id_canal=" + id_canal + ", id_contenido=" + id_contenido + ", rating=" + rating + '}';
    }
    
    
    
    
}