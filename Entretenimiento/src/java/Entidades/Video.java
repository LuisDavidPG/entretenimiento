package Entidades;


public class Video extends Contenido{
    
    private int idVideo;
    private String titulo;
    private int duracion;
    private String autor;
    private String genero;
    private String rutaVideo;
    private int id_canal;
    private int id_contenido;
    private int rating;

    public Video() {
    }

    public Video(int idVideo, String titulo, int duracion, String autor, String genero, String rutaVideo) {
        this.idVideo = idVideo;
        this.titulo = titulo;
        this.duracion = duracion;
        this.autor = autor;
        this.genero = genero;
        this.rutaVideo = rutaVideo;
    }

    

    public int getIdVideo() {
        return idVideo;
    }

    public void setIdVideo(int idVideo) {
        this.idVideo = idVideo;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public int getDuracion() {
        return duracion;
    }

    public void setDuracion(int duracion) {
        this.duracion = duracion;
    }

    public String getAutor() {
        return autor;
    }

    public void setAutor(String autor) {
        this.autor = autor;
    }

    public String getGenero() {
        return genero;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }

    public String getRutaVideo() {
        return rutaVideo;
    }

    public void setRutaVideo(String rutaVideo) {
        this.rutaVideo = rutaVideo;
    }

    

    public String toString() {
        return "Video{" + "idVideo=" + idVideo + ", titulo=" + titulo + ", duracion=" + duracion + ", autor=" + autor + ", genero=" + genero + ", rutaVideo=" + rutaVideo + '}';
    }

    public int getId_canal() {
        return id_canal;
    }

    public void setId_canal(int id_canal) {
        this.id_canal = id_canal;
    }

    public int getId_contenido() {
        return id_contenido;
    }
    
    public void setId_contenido(int id_contenido) {
        this.id_contenido = this.idVideo;
    }

  
    public int getRating() {
        return this.rating;
    }

   
    public void setRating(int rating) {
        this.rating = rating;
    }
    
    
    
    
}
