/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entidades;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Daniel_Hernandez
 */
public class BaseDatos {

    Connection con;

    public BaseDatos() {
        this.con = conexion();
    }

    public Connection conexion(){

        try {
            Class.forName("com.mysql.jdbc.Driver");
            Connection con = DriverManager.getConnection("jdbc:mysql://localhost/" + "entretenimiento?serverTimezone=UTC&useSSL=false", "root", "root");
            return con;
        } catch (SQLException ex) {
            ex.printStackTrace();
        }catch (ClassNotFoundException ex) {
             Logger.getLogger(BaseDatos.class.getName()).log(Level.SEVERE, null, ex);
         }
        return null;
    }

    public Connection getCon() {
        return con;
    }
    
    

    public void cerrarConexion() throws SQLException {
        con.close();
    }

    @Override
    public String toString() {
        return "BaseDatos{" + "con=" + con + '}';
    }

    
}
