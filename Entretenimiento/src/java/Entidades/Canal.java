package Entidades;

import java.util.ArrayList;

public class Canal {

    private int idCanal;
    private String titulo;
    private String idioma;
    private float rating;
    private String ruta_imagen;
    private ArrayList<Canal> listaSubCanales;
    private ArrayList<Contenido> listaContenidos;

    public Canal() {
    }

    public Canal(int idCanal, String titulo, String idioma, String ruta_imagen, ArrayList<Canal> listaSubCanales) {
        this.idCanal = idCanal;
        this.titulo = titulo;
        this.idioma = idioma;
        this.listaSubCanales = listaSubCanales;
        this.rating = calcularRating(listaSubCanales);
        this.ruta_imagen = ruta_imagen;
       
    }

    public Canal(int idCanal, String titulo, String idioma, ArrayList<Contenido> listaContenidos, String ruta_imagen) {
        this.idCanal = idCanal;
        this.titulo = titulo;
        this.idioma = idioma;
        this.listaContenidos = listaContenidos;
        this.rating = calcularRating();
        this.ruta_imagen = ruta_imagen;

    }

    public int getIdCanal() {
        return idCanal;
    }

    public void setIdCanal(int idCanal) {
        this.idCanal = idCanal;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getIdioma() {
        return idioma;
    }

    public void setIdioma(String idioma) {
        this.idioma = idioma;
    }

    public float getRating() {
        return rating;
    }

    public ArrayList<Canal> getListaSubCanales() {
        return listaSubCanales;
    }

    public void setListaSubCanales(ArrayList<Canal> listaSubCanales) {
        this.listaSubCanales = listaSubCanales;
    }

    public ArrayList<Contenido> getListaContenidos() {
        return listaContenidos;
    }

    public void setListaContenidos(ArrayList<Contenido> listaContenidos) {
        this.listaContenidos = listaContenidos;
    }

    public String getRuta_imagen() {
        return ruta_imagen;
    }

    public void setRuta_imagen(String ruta_imagen) {
        this.ruta_imagen = ruta_imagen;
    }

    
    
    private float calcularRating() {

        float rating = 0f;
        float incrementar = 0f;
        int contador = 0;
        
        ArrayList<Contenido> listaContenidos = getListaContenidos();

        if (!(listaContenidos.isEmpty())) {

            for (Contenido contenido : listaContenidos) {
                incrementar = incrementar + (float)contenido.getRating();
                contador++;
            }
            rating = incrementar / contador;

        } 

        return rating;
    }
    
    private float calcularRating(ArrayList<Canal> listaCanales) {

        float rating = 0f;
        float incrementar = 0f;
        int contador = 0;
        
   
        if (!(listaCanales.isEmpty())) {

            for (Canal canal : listaCanales) {
                incrementar = incrementar + (float)canal.getRating();
                contador++;
            }
            rating = incrementar / contador;
        } 

        return rating;
    }
   

}
