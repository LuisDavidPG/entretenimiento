package BaseDeDatos;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;


public class BaseDatos {

    Connection con;

    public BaseDatos() {
        this.con = conexion();
    }

    public Connection conexion() {

        try {
            Class.forName("com.mysql.jdbc.Driver");
            Connection con = DriverManager.getConnection("jdbc:mysql://localhost/" + "beat-u?serverTimezone=UTC", "root", "root");
            return con;
        } catch (SQLException ex) {
            ex.printStackTrace();
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(BaseDatos.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return null;
    }

    public void cerrarConexion() throws SQLException {
        con.close();
    }

}

    
